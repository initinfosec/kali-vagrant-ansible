# kali-vagrant-ansible

Project to leverage vagrant and ansible to create on-demand kali VMs with set preferences, apps, etc. for easy creation and teardown with minimal reconfig time. Credit to [b4ndit](https://github.com/b4ndit) for [initial work](https://github.com/b4ndit/Kali-Vagrant) and collab
